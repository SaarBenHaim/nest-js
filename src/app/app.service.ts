import { Injectable } from '@nestjs/common';
import { config } from 'dotenv';

@Injectable()
export class AppService {
  getConfig(): Record<string, unknown> {
    const appConfig = config();

    if (appConfig.error) {
      throw new Error("Error while loading config");
    }

    return appConfig.parsed;
  }
}
