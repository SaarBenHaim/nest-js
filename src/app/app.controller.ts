import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags("config")
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get("config")
  getConfig(): Record<string, unknown> {
    return this.appService.getConfig();
  }
}
